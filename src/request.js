`use strict`;

const axios = require(`axios`);

module.exports = {
    getRequest: (link) =>
        new Promise((resolve, reject) =>
            axios
                .get(link)
                .then((response) => resolve(response.data))
                .catch((catchErr) => reject(catchErr))
        ),
};
