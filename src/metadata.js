`use strict`;

const cheerio = require(`cheerio`);

module.exports = (html) => {
    const $ = cheerio.load(html);
    return {
        image: getImage($),
        title: getTitle($),
        description: getDescription($),
        link: getLink($),
    };
};

const getTitle = ($) => {
    /**
     * Open Graph meta tag for title
     */
    const ogTitle = $(`meta[property="og:title"]`).attr(`content`);
    if (ogTitle && typeof ogTitle == `string` && ogTitle.trim()) {
        return ogTitle.trim();
    }
    /**
     * Twitter card meta tag for title
     */
    const twitterTitle = $(`meta[name="twitter:title"]`).attr(`content`);
    if (
        twitterTitle &&
        typeof twitterTitle == `string` &&
        twitterTitle.trim()
    ) {
        return twitterTitle.trim();
    }
    /**
     * Schema.org meta tag for title
     */
    const schemaTitle = $(`meta[itemprop="name"]`).attr(`content`);
    if (schemaTitle && typeof schemaTitle == `string` && schemaTitle.trim()) {
        return schemaTitle.trim();
    }
    /** Title Tag */
    const titleTag = $(`title`).text();
    if (titleTag && typeof titleTag == `string` && titleTag.trim()) {
        return titleTag.trim();
    }
    return ``;
};

const getImage = ($) => {
    /**
     * Open Graph meta tag for image
     */
    const ogImage = $(`meta[property="og:image"]`).attr(`content`);
    if (ogImage && typeof ogImage == `string` && ogImage.trim()) {
        return ogImage.trim();
    }
    /**
     * Twitter card meta tag for image
     */
    const twitterImg = $(`meta[name="twitter:image"]`).attr(`content`);
    if (twitterImg && typeof twitterImg == `string` && twitterImg.trim()) {
        return twitterImg.trim();
    }
    /**
     * Schema.org meta tag for image
     */
    const schemaImg = $(`meta[itemprop="image"]`).attr(`content`);
    if (schemaImg && typeof schemaImg == `string` && schemaImg.trim()) {
        return schemaImg.trim();
    }
    return ``;
};

const getDescription = ($) => {
    /**
     * Open Graph meta tag for description
     */
    const ogDescription = $(`meta[property="og:description"]`).attr(`content`);
    if (
        ogDescription &&
        typeof ogDescription == `string` &&
        ogDescription.trim()
    ) {
        return ogDescription.trim();
    }
    /**
     * Twitter card meta tag for description
     */
    const twitterDescription = $(`meta[name="twitter:description"]`).attr(
        `content`
    );
    if (
        twitterDescription &&
        typeof twitterDescription == `string` &&
        twitterDescription.trim()
    ) {
        return twitterDescription.trim();
    }
    /**
     * Schema.org meta tag for description
     */
    const schemaDescription = $(`meta[itemprop="description"]`).attr(`content`);
    if (
        schemaDescription &&
        typeof schemaDescription == `string` &&
        schemaDescription.trim()
    ) {
        return schemaDescription.trim();
    }
    /**
     * Ordinary description tags
     */
    const nameDescription = $(`meta[name="description"]`).attr(`content`);
    if (
        nameDescription &&
        typeof nameDescription == `string` &&
        nameDescription.trim()
    ) {
        return nameDescription.trim();
    }
    const propertyDescription = $(`meta[property="description"]`).text();
    if (
        propertyDescription &&
        typeof propertyDescription == `string` &&
        propertyDescription.trim()
    ) {
        return propertyDescription.trim();
    }
    return ``;
};

const getLink = ($) => {
    /**
     * Open Graph meta tag for URL
     */
    const ogURL = $(`meta[property="og:url"]`).attr(`content`);
    if (ogURL && typeof ogURL == `string` && ogURL.trim()) {
        return ogURL.trim();
    }
    /**
     * Twitter card meta tag for URL
     */
    const twitterURL = $(`meta[name="twitter:url"]`).attr(`content`);
    if (twitterURL && typeof twitterURL == `string` && twitterURL.trim()) {
        return twitterURL.trim();
    }
    return ``;
};
